<?php


namespace App\Admin\Actions;

use App\Models\NewsImage;
use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DisableDoctor extends RowAction
{

    public $name = 'Сделать неактивным';

    public function handle(Model $model)
    {
        $model->visible = false;
        $model->save();

        return $this->response()->warning('Доктор неактивен.')->refresh();
    }


}

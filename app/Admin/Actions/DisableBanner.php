<?php


namespace App\Admin\Actions;

use App\Models\NewsImage;
use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DisableBanner extends RowAction
{

    public $name = 'Сделать неактивным';

    public function handle(Model $model)
    {
        $model->visible = false;
        $model->save();

        return $this->response()->warning('Баннер неактивен.')->refresh();
    }


}

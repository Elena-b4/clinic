<?php

namespace App\Admin\Actions;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;

class AbleBanner extends RowAction
{
    public $name = 'Сделать активным';

    public function handle(Model $model)
    {
        $model->visible = true;
        $model->save();

        return $this->response()->success('Баннер активен.')->refresh();
    }


}

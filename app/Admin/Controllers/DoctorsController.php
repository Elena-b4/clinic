<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\AbleDoctor;
use App\Admin\Actions\DisableDoctor;
use App\Models\Doctor;
use App\Traits\SortTrait;
use Encore\Admin\Actions\Action;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class DoctorsController extends AdminController
{
    use SortTrait;

    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Доктора';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Doctor());

        $grid->column('id', __('ID'))->sortable();
        $grid->column('surname', __('Фамилия'));
        $grid->column('firstname', __('Имя'));
        $grid->column('patronymic', __('Отчество'));
        $grid->column('image', __('Аватар'))->image();
        $grid->column('position', __('Должность'));
        $grid->column('visible', __('Видимость'))->bool();
        $grid->actions(function ($actions) {
            $actions->disableEdit();
            if ($actions->row->visible) {
                $actions->add(new DisableDoctor());
            } else {
                $actions->add(new AbleDoctor());
            }
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Doctor::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('surname', __('Фамилия'));
        $show->field('firstname', __('Имя'));
        $show->field('patronymic', __('Отчество'));
        $show->field('position', __('Должность'));
        $show->field('image', __('Аватар'))->image();
        $show->field('description', __('Описание'));
        $show->visible('Видимость')->using(['0' => 'нет', '1' => 'да']);

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Doctor());

        $form->display('id', __('ID'));
        $form->text('surname', __('Фамилия'))->required();
        $form->text('firstname', __('Имя'))->required();
        $form->text('patronymic', __('Отчество'));
        $form->text('position', __('Должность'));
        $form->image('image', __('Аватар'));
        $form->radio('visible', 'Видимость')
            ->options([
                0 => 'нет',
                1 => 'да',
            ])->when(0, function (Form $form) {
            })->when(1, function (Form $form) {
            })->required();
        $form->textarea('description', __('Описание'));
        $this->sort($form, 'Doctor');

        return $form;
    }
}

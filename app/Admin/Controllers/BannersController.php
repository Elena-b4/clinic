<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\AbleBanner;
use App\Admin\Actions\DisableBanner;
use App\Models\Banner;
use App\Traits\SortTrait;
use Encore\Admin\Actions\Action;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class BannersController extends AdminController
{
    use SortTrait;

    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Баннеры';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Banner());

        $grid->column('id', __('ID'))->sortable();
        $grid->column('title', __('Заголовок'));
        $grid->column('short_description', __('Краткое описание'));
        $grid->column('image', __('Картинка'))->image();
        $grid->column('visible', __('Видимость'))->bool();
        $grid->actions(function ($actions) {
            $actions->disableEdit();
            if ($actions->row->visible) {
                $actions->add(new DisableBanner());
            } else {
                $actions->add(new AbleBanner());
            }
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed   $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Banner::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('title', __('Заголовок'));
        $show->field('short_description', __('Краткое описание'));
        $show->field('description', __('Описание'));
        $show->field('image', __('Картинка'));
        $show->visible('Видимость')->using(['0' => 'нет', '1' => 'да']);

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Banner());

        $form->display('id', __('ID'));
        $form->text('title', __('Заголовок'))->required();
        $form->text('short_description', __('Краткое описание'))->required();
        $form->textarea('description', __('Описание'))->required();
        $form->image('image', __('Картинка'))->required();
        $form->radio('visible', 'Видимость')
            ->options([
                0 => 'нет',
                1 => 'да',
            ])->when(0, function (Form $form) {
            })->when(1, function (Form $form) {
            })->required();
        $this->sort($form, 'Banner');

        return $form;
    }
}

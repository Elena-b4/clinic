<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Doctor;
use Illuminate\Http\Request;

class MainController extends Controller
{

    public function getMainPage()
    {
        $banners = Banner::where('visible', 1)->get();
        $doctors = Doctor::where('visible', 1)->get();

        abort(404, 'Whatever you were looking for, look somewhere else');

        return view('main.index')->with(compact('banners', 'doctors'));
    }

    public function getAboutAsPage()
    {
        return view('about-us.index');
    }
}

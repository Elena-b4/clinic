<?php


namespace App\Traits;


trait SortTrait
{
    public $namespace = 'App\Models\\';

    public function sort($form, $model)
    {
        $model = $this->namespace . $model;
        $sortValuesArray = range(0, 0);
        $lastProductsSortValue = $model::all()->sortBy('sorting')->last();
        if ($lastProductsSortValue) {
            $sortValuesArray = range(0, ++$lastProductsSortValue->sorting);
            $form->submitted(function () use ($lastProductsSortValue, $model) {
                if (request('sorting') !== (++$lastProductsSortValue->sorting)) {
                    $nextProducts = $model::where('sorting', '>=', request('sorting'))->get();
                    foreach ($nextProducts as $nextProduct) {
                        $nextProduct->sorting = $nextProduct->sorting + 1;
                        $nextProduct->save();
                    }
                }
            });
        }

        $form->select('sorting', __('sorting'))
            ->options($model::all(['id', 'sorting'])->pluck('sorting', 'id'))->required()->default(0)
            ->options($sortValuesArray);
    }
}

@extends('layout.base')
@push('css')
    <style>

    </style>
@endpush

@section('content')
    @include('main.banners', $banners)
    @include('main.about-us')
    @include('main.directions')
    @include('main.doctors', $doctors)
    @include('main.appointment-form')
    @include('main.contacts')
@endsection
@push('js')
    <script>

    </script>

@endpush

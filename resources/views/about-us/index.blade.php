@extends('layout.base')
@push('css')
    <style>

    </style>
@endpush

@section('content')
    @include('about-us.about-us')
    @include('about-us.information')
    @include('about-us.licenses')
    @include('about-us.organizations')
@endsection
@push('js')
    <script>

    </script>

@endpush
